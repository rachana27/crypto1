#ifndef MINCKLERCRYPTO_H
#define MINCKLERCRYPTO_H

#include "Crypto.h"

class MincklerCrypto : public Crypto {
    public:
        MincklerCrypto();
        virtual ~MincklerCrypto();
		
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // MINCKLERCRYPTOIMPL_H