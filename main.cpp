// Emily Blatz Homework 4

#include <iostream>
#include <memory>

#include "include/Crypto.h"
#include "include/CryptoImpl.h"
#include "include/CryptoChain.h"
#include "include/CryptoMethod.h"
#include "include/MincklerCrypto.h

using namespace std;

int main() {

    CryptoChain cc;

    auto_ptr<CryptoMethod> c1(new CryptoMethod);
    auto_ptr<CryptoMethod> c2(new MincklerCrypto);
    auto_ptr<CryptoMethod> c3(new CryptoMethod);

    cc.add(c1.get());
    cc.add(c2.get());
    cc.add(c3.get());
    try {
        string input = "Hello, World!";
        string encoded = cc.encode(input);
        string decoded = cc.decode(encoded);
        cout << input << " -> " << encoded << " -> " << decoded << endl;
        throw exception();
    }
    catch(exception &) {
        cout << "Exception handled in the main" << endl;
    }
}
