#include "../include/MincklerCrypto.h"

#include <string>

using namespace std;

MincklerCrypto::MincklerCrypto() {

}

MincklerCrypto::~MincklerCrypto() {

}

// Override the pure virtual functions from the Crypto class.

string MincklerCrypto::encode(string s) {
	for(int i=0; i < s.length(); i++){
		s[i] = s[i] ^ (523*(i+1)) % 255;
	}
	return s;
}

string MincklerCrypto::decode(string s) {
	for(int i=0; i < s.length(); i++){
		s[i] = s[i] ^ (523*(i+1)) % 255;
	}
    return s;
}
