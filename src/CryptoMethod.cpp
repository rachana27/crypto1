// Emily Blatz Homework 4

#include "../include/CryptoMethod.h"
#include "../include/Crypto.h"
#include <string>

using namespace std;

CryptoMethod::CryptoMethod() : Crypto()
{
    //ctor
}

CryptoMethod::~CryptoMethod()
{
    //dtor
}

// alternates between upper and lower case, reverses string's character order
string CryptoMethod::encode(string s) {
    for(string::size_type i=0; i<s.length(); i++) {
        s[i] = tolower(s[i]);
        if (i%2 == 0) {
            s[i] = toupper(s[i]);
        }
    }
    for(string::size_type j=0; j<(s.length()/2); j++) {
        char temp = s[j];
        s[j] = s[s.length()-j-1];
        s[s.length()-j-1] = temp;
    }
    return s;
}

string CryptoMethod::decode(string s) {
    for(string::size_type i=0; i<(s.length()/2); i++) {
        char temp = s[i];
        s[i] = s[s.length()-i-1];
        s[s.length()-i-1] = temp;
    }
    for(string::size_type j=0; j<s.length(); j++) {
        if (j%2 == 0) {
            s[j] = tolower(s[j]);
        }
    }
    s[0] = toupper(s[0]);
    s[7] = toupper(s[7]);
    return s;
}
